package eu.europa.ec.sante.openncp.application.client.connector;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"eu.europa.ec.sante.openncp"})
public class SpringConfiguration {
}
