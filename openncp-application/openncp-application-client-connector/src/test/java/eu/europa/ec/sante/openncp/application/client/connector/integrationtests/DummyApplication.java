package eu.europa.ec.sante.openncp.application.client.connector.integrationtests;

import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication(scanBasePackages = "eu.europa.ec.sante.openncp")
public class DummyApplication {}
